import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'backlog',
    loadChildren: () => import('./modules/backlog/backlog.module').then((module) => module.BacklogModule)
  },
  {
    path: 'board',
    loadChildren: () => import('./modules/board/board.module').then((module) => module.BoardModule)
  },
  {
    path: 'burn-down',
    loadChildren: () => import('./modules/burn-down/burn-down.module').then((module) => module.BurnDownModule)
  },
  {
    path: 'burn-up',
    loadChildren: () => import('./modules/burn-up/burn-up.module').then((module) => module.BurnUpModule)
  },
  {
    path: '',
    redirectTo: 'board',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
