import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BoardListComponent} from './components/board-list/board-list.component';

const routes: Routes = [
  {
    path: 'list',
    component: BoardListComponent
  },
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BoardRoutingModule { }
