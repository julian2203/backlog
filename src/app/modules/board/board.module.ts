import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';

import { BoardRoutingModule } from './board-routing.module';
import { BoardListComponent } from './components/board-list/board-list.component';

@NgModule({
  declarations: [
    BoardListComponent
  ],
  imports: [
    CommonModule,
    BoardRoutingModule,
    DragDropModule,
    MatIconModule,
    MatChipsModule,
    MatCardModule,
    MatExpansionModule
  ]
})
export class BoardModule { }
