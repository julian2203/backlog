import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ISprint } from 'src/app/models/sprint';

@Component({
  selector: 'app-board-list',
  templateUrl: './board-list.component.html',
  styleUrls: ['./board-list.component.scss']
})
export class BoardListComponent implements OnInit {

  public sprints: ISprint[];

  public constructor(
    private httpClient: HttpClient
  ) { }

  // tslint:disable: prefer-for-of
  public ngOnInit(): void {
    this.httpClient.get('http://localhost:8080/sprints').subscribe((data: any): void => {
      this.sprints = data._embedded.sprints.sort((a: ISprint, b: ISprint): number => b.priority - a.priority);

      this.sprints.pop();

      for (let index: number = 0; index < this.sprints.length; index++) {

        this.sprints[index]['todo'] = [];
        this.sprints[index]['inprogress'] = [];
        this.sprints[index]['done'] = [];

        for (let secondIndex: number = 0; secondIndex < this.sprints[index].backlogs.length; secondIndex++) {

          if (this.sprints[index].backlogs[secondIndex].state === 'TO DO') {
            this.sprints[index]['todo'].push(this.sprints[index].backlogs[secondIndex]);
          }

          if (this.sprints[index].backlogs[secondIndex].state === 'IN PROGRESS') {
            this.sprints[index]['inprogress'].push(this.sprints[index].backlogs[secondIndex]);
          }

          if (this.sprints[index].backlogs[secondIndex].state === 'DONE') {
            this.sprints[index]['done'].push(this.sprints[index].backlogs[secondIndex]);
          }

        }
      }

    });
  }

  public drop(event: CdkDragDrop<string[]>): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

}
