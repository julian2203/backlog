import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BurnDownComponent } from './components/burn-down/burn-down.component';

const routes: Routes = [
  {
    path: '',
    component: BurnDownComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BurnDownRoutingModule { }
