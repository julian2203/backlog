import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { BurnDownRoutingModule } from './burn-down-routing.module';
import { BurnDownComponent } from './components/burn-down/burn-down.component';

@NgModule({
  declarations: [
    BurnDownComponent
  ],
  imports: [
    CommonModule,
    BurnDownRoutingModule
  ]
})
export class BurnDownModule { }
