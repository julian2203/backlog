import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { BurnUpRoutingModule } from './burn-up-routing.module';
import { BurnUpComponent } from './components/burn-up/burn-up.component';

@NgModule({
  declarations: [
    BurnUpComponent
  ],
  imports: [
    CommonModule,
    BurnUpRoutingModule
  ]
})
export class BurnUpModule { }
