import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ISprint } from 'src/app/models/sprint';

@Component({
  selector: 'app-burn-up',
  templateUrl: './burn-up.component.html',
  styleUrls: ['./burn-up.component.scss']
})
export class BurnUpComponent implements OnInit {

  public chart: any = undefined;

  public sprints: ISprint[];

  public constructor(
    private httpClient: HttpClient
  ) { }

  public ngOnInit(): void {
    this.httpClient.get('http://localhost:8080/sprints').subscribe((data: any): void => {
      this.sprints = data._embedded.sprints.sort((a: ISprint, b: ISprint): number => b.priority - a.priority);

      this.chart = new Chart('burn-down', {
        type: 'line',
        data: {
          labels: ['09 Nov', '10 Nov', '11 Nov', '12 Nov', '13 Nov', '14 Nov', '15 Nov', '16 Nov', '17 Nov', '18 Nov', '19 Nov'],
          datasets: [
            {
              label: 'Estimado',
              fill: false,
              lineTension: 0.0,
              data: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            },
            {
              label: 'Progreso',
              fill: false,
              backgroundColor: '#FF0000',
              borderColor: '#FF0000',
              lineTension: 0.0,
              data: [0, 3, 4, 4, 5, 5]
            }
          ]
        },
        options: {
          legend: {
            position: 'bottom'
          },
          scales: {
            xAxes: [
              {
                scaleLabel: {
                  display: true,
                  labelString: 'Tiempo'
                }
              }
            ],
            yAxes: [
              {
                scaleLabel: {
                  display: true,
                  labelString: 'Incidencias'
                },
                ticks: {
                  min: 0,
                  max: 10
                }
              }
            ]
          },
          title: {
            display: true,
            text: `Burn Up ${this.sprints[0].name}`
          }
        }
      });
    });
  }

}
