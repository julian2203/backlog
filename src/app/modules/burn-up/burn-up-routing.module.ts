import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BurnUpComponent } from './components/burn-up/burn-up.component';

const routes: Routes = [
  {
    path: '',
    component: BurnUpComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BurnUpRoutingModule { }
