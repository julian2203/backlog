import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { IBacklog } from 'src/app/models/backlog';
import { ISprint } from 'src/app/models/sprint';

import { BacklogCreateComponent } from '../backlog-create/backlog-create.component';

@Component({
  selector: 'app-backlog-list',
  templateUrl: './backlog-list.component.html',
  styleUrls: ['./backlog-list.component.scss']
})
export class BacklogListComponent implements OnInit {

  public sprints: ISprint[];

  public constructor(
    private httpClient: HttpClient,
    private dialog: MatDialog
  ) { }

  public ngOnInit(): void {
    this.httpClient.get('http://localhost:8080/sprints').subscribe((data: any): void => {
      this.sprints = data._embedded.sprints.sort((a: ISprint, b: ISprint): number => b.priority - a.priority);
    });
  }

  public drop(event: CdkDragDrop<string[]>): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }

  public onCreate(): void {
    const dialogRef: MatDialogRef<BacklogCreateComponent, any> = this.dialog.open(
      BacklogCreateComponent,
      {
        width: '600px'
      }
    );
    dialogRef.afterClosed().subscribe((result: IBacklog): void => {
      if (result) {
        console.log(result);
        this.sprints.filter((sprint: ISprint): any => sprint.priority === 0)[0].backlogs.push(result);
      }
    });
  }

}
