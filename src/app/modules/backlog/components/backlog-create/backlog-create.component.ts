import { state } from '@angular/animations';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ICustomer } from 'src/app/models/customer';

import { IBacklog } from '../../../../models/backlog';
import { IBacklogType } from '../../../../models/backlog-type';

@Component({
  selector: 'app-backlog-create',
  templateUrl: './backlog-create.component.html',
  styleUrls: ['./backlog-create.component.scss']
})
export class BacklogCreateComponent implements OnInit {

  public backlogTypes: IBacklogType[];
  public customers: ICustomer[];

  public backlogForm: FormGroup;

  public constructor(
    private httpClient: HttpClient,
    public dialogRef: MatDialogRef<BacklogCreateComponent>
  ) { }

  public ngOnInit(): void {
    this.httpClient.get('http://localhost:8080/backlogTypes').subscribe((data: any): void => {
      this.backlogTypes = data._embedded.backlogTypes;
    });
    this.httpClient.get('http://localhost:8080/customers').subscribe((data: any): void => {
      this.customers = data._embedded.customers;
    });

    this.backlogForm = new FormGroup({
      type: new FormControl('', { validators: [Validators.required] }),
      name: new FormControl('', { validators: [Validators.required] }),
      customer: new FormControl('', { validators: [Validators.required] }),
      estimatedTime: new FormControl('', { validators: [Validators.required, Validators.min(1)] })
    });
  }

  public onSubmit(): void {
    this.closeDialog();
  }

  public closeDialog(): void {
    const backlog: IBacklog = {
      backlogType: this.backlogForm.get('type').value,
      name: this.backlogForm.get('name').value,
      customer: this.backlogForm.get('customer').value,
      estimatedTime: this.backlogForm.get('estimatedTime').value,
      state: 'TO DO'
    };
    this.dialogRef.close(backlog);
  }

}
