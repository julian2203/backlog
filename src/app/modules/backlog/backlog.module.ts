import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';

import { BacklogRoutingModule } from './backlog-routing.module';
import { BacklogCreateComponent } from './components/backlog-create/backlog-create.component';
import { BacklogListComponent } from './components/backlog-list/backlog-list.component';

@NgModule({
  declarations: [
    BacklogCreateComponent,
    BacklogListComponent
  ],
  imports: [
    BacklogRoutingModule,
    CommonModule,
    DragDropModule,
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatDialogModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatListModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    BacklogCreateComponent
  ]
})
export class BacklogModule { }
