import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BacklogListComponent } from './components/backlog-list/backlog-list.component';

const routes: Routes = [
  {
    path: 'list',
    component: BacklogListComponent
  },
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BacklogRoutingModule { }
