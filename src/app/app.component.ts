import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.matIconRegistry.addSvgIcon(
      'bug',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/img/bug.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'story',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/img/story.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'task',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/img/task.svg')
    );
  }

}
