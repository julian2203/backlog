export interface IBacklogType {
    name: string;
    icon: string;
}
