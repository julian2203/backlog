import { IBacklogType } from './backlog-type';
import { ICustomer } from './customer';

export interface IBacklog {
    backlogType?: IBacklogType;
    customer?: ICustomer;
    name?: string;
    estimatedTime?: number;
    state?: string;
}
