import { IBacklog } from './backlog';

export interface ISprint {
    priority: number;
    name: string;
    backlogs: IBacklog[];
}
