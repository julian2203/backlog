export interface ICustomer {
    name: string;
    role: string;
    image: string;
}
